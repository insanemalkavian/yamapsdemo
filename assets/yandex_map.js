/*
 * Copyright 2010-2011 Research In Motion Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// initialize the map
var markersArray = [];

var myMap;

function init(){ 
    myMap = new ymaps.Map ("map", {
        center: [55.76, 37.64],
        zoom: 7
    });
    
    myMap.events.add('click', clicked);
    
    createPushPin(55.76, 37.64, 'Ballonnnnn', 'path');
}

function initMaps() {
    console.log("init YandexMaps");
    ymaps.ready(init);
    console.log("Done init YandexMaps");
}

function clicked(e) {
    var position = e.get('coordPosition');
    myMap.geoObjects.add(new ymaps.Placemark(position));

    //var location = bingMap.tryPixelToLocation(new Microsoft.Maps.Point(e.getX(), e.getY()), Microsoft.Maps.PixelReference.page);
    //navigator.cascades.postMessage("clicked:" + location.latitude + "," + location.longitude + "," + e.pageX + "," + e.pageY);
}

function centerChanged() {
    //var point = bingMap.tryLocationToPixel(bingMap.getCenter(), Microsoft.Maps.PixelReference.page);
    //navigator.cascades.postMessage("centerChanged:" + bingMap.getCenter().latitude + "," + bingMap.getCenter().longitude + "," + point.x + "," + point.y);

}

function onMessage(message) {

}

function setZoom(zoomLevel) {
	myMap.setZoom(zoomLevel/*, {duration: 1000}*/);
}

function zoomIn() {
    setZoom(myMap.getZoom() + 1);
}

function zoomOut() {
    setZoom(myMap.getZoom() - 1);
}

function setCenter(lat, lon) {
	myMap.setCenter([lat, lon]);
}

function setMapTypeId(mapType) {
    //var options = bingMap.getOptions();
    //options.mapTypeId = mapType;
    //bingMap.setView(options);
}

// create a marker / push-pin
function createPushPin(lat, lon, title, iconpath) {
    var myPlacemark = new ymaps.Placemark([lat, lon], {
        hintContent: 'hintContent!',
        balloonContent: title
    });
    
    myMap.geoObjects.add(myPlacemark);

    //var pin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(lat, lon), {icon:iconpath, height:60, width:60, anchor:new Microsoft.Maps.Point(20,58), draggable: true});
    //bingMap.entities.push(pin);
    //Microsoft.Maps.Events.addHandler(pin, 'click', markerClicked);
    //markersArray.push(pin);
}

function removeAllPins() {
	myMap.geoObjects.removeAll();
    //if (markersArray) {
    //    for (i in markersArray) {
    //        bingMap.entities.remove(markersArray[i])
    //    }
    //}
}

function markerClicked(e) {
    //var pinLoc = e.target.getLocation();
    //var point = bingMap.tryLocationToPixel(pinLoc, Microsoft.Maps.PixelReference.page);
    //navigator.cascades.postMessage("markerClicked:" + pinLoc.latitude + "," + pinLoc.longitude + "," + point.x + "," + point.y);
}
